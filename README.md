# La gittérature

La *gittérature* découvre sa [cyberpoétique](https://www.cyberpoetique.org/gitterature), et se découvre une [dynamique nouvelle](https://antilivre.gitlab.io/gitterature/).

## Explication

La *gittérature* est l'enfantement de l'essaim qui s'échafaude par la circulation de l'information. La création y est re-création, *récréation* des formes qui se nourrissent d'elles-mêmes, dans la joie de voir se développer l'espace-temps de leur devenir.

## Outils

La *gittérature* est l'adjonction de la littérature aux potentialités de l'outil libre [Git](https://fr.wikipedia.org/wiki/Git).

Nous saupoudrons sur cette rencontre quelques lignes de [Bash](https://fr.wikipedia.org/wiki/Bourne-Again_shell). (Pour un peu de *gittérature bashing...*)

Pour mettre en mouvement ce projet, il vous suffit d'installer Git, de cloner ce dépôt `git clone https://gitlab.com/antilivre/gitterature/`, et d'écrire une des commandes suivantes, dans un terminal, depuis le dossier `gitterature`. (Si la commande n'est pas *exécutable*, il suffit, dans le même dossier, d'écrire depuis un terminal `chmod +x gitterature.sh`.)

## Commandes

Quelques commandes pour qu'apparaissent les mystères du texte. (La commande `ctrl-c` permet de faire cesser la commande — et avec cela, la stridence de la lettre.)

- `./gitterature.sh chant` : faire apparaître **le sonnet cassé**
- `./gitterature.sh chant-chaos` : faire apparaître **le désordre du sonnet**
- `./gitterature.sh chant-brisure` : faire apparaître **la fracture du sonnet**
- `./gitterature.sh chant-infini` : faire apparaître **l’infini d’un sonnet brisé**
- `./gitterature.sh chant-fusion` : faire apparaître **la transformation du sonnet**
- `./gitterature.sh bloc` : faire apparaître **le bloc noir**
- `./gitterature.sh bloc-chaos` : faire apparaître  **le désordre du bloc noir**
- `./gitterature.sh bloc-infini` : faire apparaître **l’infini du bloc noir**
- `./gitterature.sh situation` : faire apparaître **de la théorie**
- `./gitterature.sh secret` : faire apparaître **le hachage**
- `./gitterature.sh fuite` : faire apparaître **la fuite des heures** 
- `./gitterature.sh dynamite` : faire apparaître **le déploiement des réseaux**
- `./gitterature.sh {tsimtsoum, etc.}` : faire apparaître **le vide**

## La licence

Ce projet est dédié au domaine public. Les textes et le code (ne faisant pas partie d'une librairie préexistante) sont mis à disposition selon les termes de la Licence Creative&nbsp;Commons&nbsp;Zero (CC&nbsp;1.0 Universel) — voir le fichier [LICENSE](LICENSE) à cet effet, ainsi que notre [politique](https://abrupt.ch/partage/) de partage.

## La mauvaise influence...

...est celle de [Louis-Olivier Brassard](https://scolaire.loupbrun.ca/piece01/) et d'[Antoine Fauchié](https://www.quaternum.net/2019/12/09/cheminement-textuel/). Leurs projets ont été l'étincelle.

Existe aussi un [répertoire](https://framagit.org/gitterature/gitterature) des liens entre Git et littérature.



