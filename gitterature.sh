#!/bin/bash

REPO="https://gitlab.com/antilivre/gitterature"
TAG="sonnet.*"

function fin() {
  echo ""
  echo ""
  echo "Et le vide contemple $(whoami | perl -C -lne 'print ucfirst')."
}

shuffle() { perl -MList::Util=shuffle -e 'print shuffle(<>);' "$@"; }

for arg in "$@"; do
  shift
  case "$arg" in

    "chant")
      echo ''
      while IFS= read -r line || [[ -n "$line" ]]; do
        echo "$line"
        sleep 1
      done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%s" |
               sed '/^$/d')
      echo ''
    ;;

    "chant-chaos")
      echo ''
      while IFS= read -r line || [[ -n "$line" ]]; do
        echo "$line"
        sleep 1
      done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%s" |
               shuffle |
               sed '/^$/d')
      echo ''
    ;;

    "chant-brisure")
      echo ''
      while IFS= read -r line || [[ -n "$line" ]]; do
        echo "$line"
        sleep 0.5
      done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%s" |
               sed 's/\. /\./g' |
               sed 's/\./\.\
/g' |
               sed '/^$/d' |
               shuffle)
      echo ''
    ;;

    "chant-infini")
      trap fin EXIT
      echo ''
      while true
      do
        while IFS= read -r line || [[ -n "$line" ]]; do
          echo "$line"
          sleep 0.1
        done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%s" |
                 sed 's/\. /\./g' |
                 sed 's/\./\.\
/g' |
                 sed '/^$/d' |
                 shuffle)
      done
    ;;

    "chant-fusion")
      echo ''
      while IFS= read -r char || [[ -n "$char" ]]; do
        echo -n "$char"
        sleep 0.5
      done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%s" |
               sed 's/\. /\./g' |
               sed 's/\./\. \
/g' |
               sed '/^$/d' |
               shuffle)
      echo ''
      echo ''
    ;;

    "bloc")
      echo ''
      while IFS= read -r line || [[ -n "$line" ]]; do
        echo "$line"
        sleep 0.5
      done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%b" |
               sed 's/$/ /g' |
               tr -d '\n' |
               fold -w 72 -s |
               sed 's/  / /g')
      echo ''
    ;;

    "bloc-chaos")
      echo ''
      while IFS= read -r line || [[ -n "$line" ]]; do
        echo "$line"
        sleep 0.5
      done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%b" |
               sed 's/$/ /g' |
               sed 's/^ //g' |
               shuffle |
               tr -d '\n' |
               fold -w 72 -s |
               sed 's/  */ /g')
      echo ''
    ;;

    "bloc-infini")
      trap fin EXIT
      echo ''
      while true
      do
        while IFS= read -rn1 char || [[ -n "$char" ]]; do
          echo -n "$char"
          sleep 0.01
        done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%b" |
                 sed 's/$/ /g' |
                 sed 's/^ //g' |
                 shuffle |
                 tr -d '\n' |
                 fold -w 72 -s |
                 sed 's/  */ /g')
      done
    ;;

    "situation")
      echo ''
      while IFS= read -r line || [[ -n "$line" ]]; do
        echo "$line"
        sleep 0.5
      done < <(fold -w 72 -s gitterature.md | sed 's/*//g')
      # rm tmp.txt
      echo ''
    ;;

    "secret")
      trap fin EXIT
      echo ''
      while true
      do
        while IFS= read -rn1 char || [[ -n "$char" ]]; do
          echo -n "$char"
          sleep 0.01
        done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%H%T" |
                 tr -d '\n')
      done
      echo ''
    ;;

    "fuite")
      trap fin EXIT
      echo ''
      while true
      do
        while IFS= read -rn1 char || [[ -n "$char" ]]; do
          echo -n "$char"
          sleep 0.1
        done < <(git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:"%ad" --date=format:"%Y%m%d%H%M%S" |
                 tr -d '\n')
      done
      echo ''
    ;;

    "dynamite")

      echo '
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Gittérature</title>
    <link rel="icon" type="image/png" href="./img/favicon.png">
    <link rel="apple-touch-icon" href="./img/favicon.png">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/gitterature.css">
  </head>
  <body>
    <main class="container">
    <div class="text text--menu" id="text-%h">
      <button class="btn hash">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
      <div class="text__container text__container--left">
        <div class="text__subject" style="display: none;">
          <div>Vous contemplez<br>le vide.<br><br>Le dépôt <a href="https://gitlab.com/antilivre/gitterature/" alt="Dépôt Git de la Gittérature">Git</a>.</div>
        </div>
      </div>
      <div class="text__container text__container--right">
        <div class="text__body" style="display: none;">
        <div>Et le vide<br>vous contemple.<br><br>Le dépôt <a href="https://www.cyberpoetique.org/gitterature" alt="Gittérature sur la Cyberpoétique">Glitch</a>.</div>
        </div>
      </div>
    </div>' > public/index.html

      git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:'
    <div class="text text--list" id="text-%h">
        <button class="btn hash">%h</button>
        <div class="text__container text__container--left">
          <div class="text__subject" style="display: none;">
            <p class="markdown">%s</p>
            <br>(Le <a href="'$REPO'/blob/%H/gitterature.md" class="text__link" target="_blank">verbe</a> %h.)
          </div>
        </div>
        <div class="text__container text__container--right">
          <div class="text__body" style="display: none;">
            <p class="markdown">%b</p>
            <br>(Le <a href="'$REPO'/commit/%H" class="text__link" target="_blank">temps</a> %h.)
          </div>
        </div>
    </div>' >> public/index.html

    echo '
    <script src="./js/commonmark.min.js"></script>
    <script src="./js/baffle.min.js"></script>
    <script src="./js/gitterature.js"></script>
    </main>
  </body>
</html>' >> public/index.html

    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      xdg-open public/index.html &
    elif [[ "$OSTYPE" == "freebsd"* ]]; then
      xdg-open public/index.html &
    elif [[ "$OSTYPE" == "darwin"* ]]; then
      open public/index.html &
    elif [[ "$OSTYPE" == "cygwin" ]]; then
      cygstart public/index.html
    elif [[ "$OSTYPE" == "msys" ]]; then
      public\index.html
    fi
    ;;

    "manifestation")

      echo '
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Gittérature</title>
    <link rel="icon" type="image/png" href="./img/favicon.png">
    <link rel="apple-touch-icon" href="./img/favicon.png">
    <link rel="stylesheet" href="./css/normalize.css">
    <link rel="stylesheet" href="./css/gitterature.css">
  </head>
  <body>
    <main class="container">
    <div class="text text--menu" id="text-%h">
      <button class="btn hash">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
      <div class="text__container text__container--left">
        <div class="text__subject" style="display: none;">
          <div>Vous contemplez<br>le vide.<br><br>Le dépôt <a href="https://gitlab.com/antilivre/gitterature/" alt="Dépôt Git de la Gittérature">Git</a>.</div>
        </div>
      </div>
      <div class="text__container text__container--right">
        <div class="text__body" style="display: none;">
        <div>Et le vide<br>vous contemple.<br><br>Le dépôt <a href="https://www.cyberpoetique.org/gitterature" alt="Gittérature sur la Cyberpoétique">Glitch</a>.</div>
        </div>
      </div>
    </div>' > public/index.html

      git log --no-walk --tags="${TAG}" --reverse --pretty=tformat:'
    <div class="text text--list" id="text-%h">
        <button class="btn hash">%h</button>
        <div class="text__container text__container--left">
          <div class="text__subject" style="display: none;">
            <p class="markdown">%s</p>
            <br>(Le <a href="'$REPO'/blob/%H/log.md" class="text__link" target="_blank">verbe</a> %h.)
          </div>
        </div>
        <div class="text__container text__container--right">
          <div class="text__body" style="display: none;">
            <p class="markdown">%b</p>
            <br>(Le <a href="'$REPO'/commit/%H" class="text__link" target="_blank">temps</a> %h.)
          </div>
        </div>
    </div>' >> public/index.html

    echo '
    <script src="./js/commonmark.min.js"></script>
    <script src="./js/baffle.min.js"></script>
    <script src="./js/gitterature.js"></script>
    </main>
  </body>
</html>' >> public/index.html
    ;;

    *)
      echo ""
      echo -n "$(whoami | perl -C -lne 'print ucfirst') contemple le vide. "
      echo "Et le vide contemple $(whoami | perl -C -lne 'print ucfirst')."
      echo ""
      exit 1
    ;;

  esac
done
